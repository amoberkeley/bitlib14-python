from bitlib import *
import matplotlib.pyplot as plt
import numpy as np
import time
import scipy.signal as signal
import math
import thread

bitscope_link = "UDP:192.168.1.73"


BL_Initialize()
  
handle = BL_OpenLink(bitscope_link)
if handle == -1:
    print("Failed to open link");
else:
    print("Link opened successfully.")
#    print("VERSION: %s" % BL_Version(BL_VERSION_DEVICE))
    BL_SelectBitScope(handle)

    BL_Mode(BL_MODE_MULTI);

    BL_ChannelEnable(0, BL_CHANNEL_BNC, 1)
    ch0_range = 2
    ch0_pp = BL_RangeValue(handle, 0, BL_SOURCE_PRESCALE1, ch0_range, 0)
    BL_SetupChannel(0, BL_CHANNEL_BNC, BL_COUPLING_DC, ch0_range, -ch0_range/2, BL_PRESCALE_ON)
    BL_ChannelEnable(1,BL_CHANNEL_BNC,1)
    
    ch1_range = 2
    ch1_pp = BL_RangeValue(handle, 1, BL_SOURCE_PRESCALE1, ch1_range, 0)
    BL_SetupChannel(1, BL_CHANNEL_BNC, BL_COUPLING_DC, ch1_range, +ch1_range/2, BL_PRESCALE_ON)

    BL_ChannelSelect(0)
    BL_CaptureTime(20000) # in uS
    BL_SampleRate(1); # in MHz
    BL_PreTrigger(0)
    samples = BL_BufferSize()

    print("Expecting %d data points." % samples)

    BL_SetupTrigger(BL_TRIG_EXT, 1, BL_TRIG_RISE)

    fig = plt.figure()
    
    data1 = np.zeros(samples)
    data2 = np.zeros(samples)
    while plt.fignum_exists(fig.number):
        BL_Trace(BL_TRACE_FOREVER, BL_ASYNCHRONOUS)       
        print('Waiting... (%d)' % BL_GetTraceState())
        while BL_GetTraceState() is BL_STATE_ARMED and plt.fignum_exists(fig.number):
            plt.pause(.1)

        if BL_GetTraceState() is BL_STATE_TIMEDOUT:
            BL_Halt(handle)
            raise Exception("FAILED: Error during trace.")

        if not plt.fignum_exists(fig.number):
            BL_Halt(handle)
            break
    
        print('Acquiring...')
        BL_ChannelSelect(0)
        BL_Acquire(samples, data1)
        BL_ChannelSelect(1)
        BL_Acquire(samples, data2)

        if not plt.fignum_exists(fig.number):
            break
        plt.cla()
        plt.plot(data1)
        plt.plot(data2)
        
        plt.draw()

BL_Close()

