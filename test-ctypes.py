import ctypes
from ctypes import c_int,c_bool,c_double,c_char_p
from ctypes.util import find_library
import matplotlib.pyplot as plt
import numpy as np
import numpy.ctypeslib as npct
import time
import scipy.signal as signal
import math
import thread

array_1d_double = npct.ndpointer(dtype=np.double, ndim=1, flags='CONTIGUOUS')

bitlib = ctypes.windll.LoadLibrary(find_library('bitlib'))
bitlib.BL_OpenLink.argtypes = [c_char_p]
bitlib.BL_OpenLink.restype = c_int
bitlib.BL_Log.restype = c_char_p
bitlib.BL_SampleRate.argtypes = [c_double]
bitlib.BL_SetupChannel.argtypes = [c_int, c_bool, c_int, c_int, c_double, c_int]
bitlib.BL_SetupTrigger.argtypes = [c_int, c_double, c_int]
bitlib.BL_Acquire.argtypes = [c_int, array_1d_double]

def print_log():
    print("\n LOG:: \n")
    log = bitlib.BL_Log()
    while log:
        print(log)
        log = bitlib.BL_Log()

print("Opening library...")
bitlib.BL_Initialize()
  
print("Opening probe file...")

if bitlib.BL_OpenLink("UDP:192.168.1.73") == -1:
    print("Failed to open link");
else:
    print("%d link(s) opened." % bitlib.BL_Count(-1))
    print("VERSION: %s" % bitlib.BL_Version(0))

    bitlib.BL_SelectBitScope(0)

    bitlib.BL_Mode(4);

    bitlib.BL_ChannelEnable(0, 0, 1)
    bitlib.BL_SetupChannel(0, 0, 1, 3, -1.15/2, 1)
    bitlib.BL_ChannelEnable(1,0,1)
    bitlib.BL_SetupChannel(1, 0, 1, 3, +1.15/2, 1)

    bitlib.BL_ChannelSelect(0)
    bitlib.BL_CaptureTime(10000) # in uS
    bitlib.BL_SampleRate(40); # in MHz
    bitlib.BL_PreTrigger(0)

    return_pts = bitlib.BL_BufferSize()

    print("Expecting %d data points." % return_pts)

    bitlib.BL_SetupTrigger(64, 0.1, 1)
    
    print_log()

    fig = plt.figure()
    data1 = np.zeros(return_pts)
    data2 = np.zeros(return_pts)
    while plt.fignum_exists(fig.number):
        bitlib.BL_Trace(-1, 1)       
        print('Waiting... (%d)' % bitlib.BL_GetTraceState())
        while bitlib.BL_GetTraceState() is 1 and plt.fignum_exists(fig.number):
            plt.pause(.1)

        if bitlib.BL_GetTraceState() is 3:
            bitlib.BL_Halt(0)
            raise Exception("FAILED: Error during trace.")

        if not plt.fignum_exists(fig.number):
            bitlib.BL_Halt(0)
            break
    #
    # Acquire (i.e. upload) the captured data (which may be less than MY_SIZE!).
    #
        print("Trace completed successfully.")
        print('Acquiring...')
        bitlib.BL_ChannelSelect(0)
        bitlib.BL_Acquire(return_pts, data1)
        bitlib.BL_ChannelSelect(1)
        bitlib.BL_Acquire(return_pts, data2)

        if not plt.fignum_exists(fig.number):
            break
        plt.subplot(2,1,1)
        plt.cla()
        plt.plot(data1)
        plt.plot(data2)
        
        plt.draw()

print_log()
bitlib.BL_Close()
print("Done.")

