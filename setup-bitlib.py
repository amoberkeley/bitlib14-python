from distutils.core import setup, Extension
import numpy as np

module = Extension ( 'bitlib', ['bitlibmodule.c'], libraries = ['BitLib'],
    include_dirs = [np.get_include()])

setup ( name = "bitlib", version = "1.4", 
        maintainer = "Jonathan Kohler", maintainer_email = "jkohler@berkeley.edu",
        description = "BitScope Library 1.4 Python Extension module",
        ext_modules = [module] )
