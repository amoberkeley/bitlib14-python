/* bitlib -- BitScope Python Library API Bindings V1.4. */

#include <stdlib.h>
#include <Python.h>
#include <arrayobject.h>
#include <bitlib.h> /* bitscope library api */

#define max(a,b) ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); _a > _b ? _a : _b; })
     
#define min(a,b) ({ __typeof__ (a) _a = (a); \
	__typeof__ (b) _b = (b); _a < _b ? _a : _b; })

#define BL_PYTHON_VERSION "BitScope Python 1.4";

static char bitlib_doc[] = "BitScope Library API 1.4";

static PyObject * bitlib_BL_Initialize ( PyObject * self, PyObject * args ) {
	BL_Initialize();
	Py_RETURN_TRUE;
}

static PyObject * bitlib_BL_Open ( PyObject * self, PyObject * args ) {
	char * probe = "bitscope.prb"; bool all = 1;
	if ( !PyArg_ParseTuple(args, "|sb:BL_Open", &probe, &all) ) return NULL;
	if ( BL_Open( probe, all )) Py_RETURN_TRUE; else Py_RETURN_FALSE;
}

static PyObject * bitlib_BL_OpenLink ( PyObject * self, PyObject * args ) {
	char * link = "";
	if ( !PyArg_ParseTuple(args, "s:BL_OpenLink", &link) ) return NULL;
	return PyInt_FromLong( (long) BL_OpenLink( link ) );
}

static PyObject * bitlib_BL_Log ( PyObject * self, PyObject * args ) {
	char* log = BL_Log();
	if (log) return PyString_FromFormat(log);
	Py_RETURN_FALSE;
}

static PyObject * bitlib_BL_Count ( PyObject * self, PyObject * args ) {
	int handle = -1;
	if ( !PyArg_ParseTuple(args, "|i:BL_Count", &handle) ) return NULL;
	return PyInt_FromLong( (long) BL_Count(handle) );
}

#define BL_VERSION_BINDING -1
#define BL_VERSION_DEVICE 0
#define BL_VERSION_LIBRARY 1

static PyObject * bitlib_BL_Version ( PyObject * self, PyObject * args ) {
	int handle = -1; char * version = BL_PYTHON_VERSION;
	if ( !PyArg_ParseTuple(args, "|i:BL_Version", &handle) ) return NULL;
	if ( handle >= 0 ) version = BL_Version(handle);
	return PyString_FromFormat( version );
}

static PyObject * bitlib_BL_Name ( PyObject * self, PyObject * args ) {
	int handle;
	char array[100];	
	if ( !PyArg_ParseTuple(args, "i:BL_Name", &handle) ) return NULL;
	return PyString_FromFormat( BL_Name(handle, array) );
}

static PyObject * bitlib_BL_SelectBitScope ( PyObject * self, PyObject * args ) {
	int handle;
	if ( !PyArg_ParseTuple(args, "i:BL_SelectBitScope", &handle) ) return NULL;
	return PyInt_FromLong( (long) BL_SelectBitScope(handle) );
}

#define BL_MODE_SINGLE	0
#define BL_MODE_CHOP	1
#define BL_MODE_MIXED	2
#define BL_MODE_LOGIC	3 
#define BL_MODE_MULTI	4 
#define BL_MODE_STREAM 	5 

static PyObject * bitlib_BL_Mode ( PyObject * self, PyObject * args ) {
	int mode;
	if ( !PyArg_ParseTuple(args, "i:BL_Mode", &mode) ) return NULL;
	if ( BL_Mode(mode) ) Py_RETURN_TRUE; else Py_RETURN_FALSE;
}

static PyObject * bitlib_BL_ChannelSelect ( PyObject * self, PyObject * args ) {
	int channel;
	if ( !PyArg_ParseTuple(args, "i:BL_ChannelSelect", &channel) ) return NULL;
	if ( BL_ChannelSelect(channel) ) Py_RETURN_TRUE; else Py_RETURN_FALSE;
}

static PyObject * bitlib_BL_ChannelEnable ( PyObject * self, PyObject * args ) {
	int channel; bool isPod; bool enable;
	if (!PyArg_ParseTuple(args, "ibb:BL_ChannelEnable", &channel, &isPod, &enable) ) return NULL;
	if ( BL_ChannelEnable(channel, isPod, enable) ) Py_RETURN_TRUE; else Py_RETURN_FALSE;
}

#define BL_CHANNEL_BNC 0
#define BL_CHANNEL_POD 1

#define BL_COUPLING_AC 0
#define BL_COUPLING_DC 1

#define BL_PRESCALE_OFF 0
#define BL_PRESCALE_ON  1
#define BL_PRESCALE_GND 2

static PyObject * bitlib_BL_SetupChannel ( PyObject * self, PyObject * args ) {
	int channel; bool isPod; int coupling; int range; double offset; int prescale;
	if ( !PyArg_ParseTuple(args, "ibiidi:BL_SetupChannel", &channel, &isPod, &coupling, &range, &offset, &prescale) ) return NULL;
	if ( BL_SetupChannel(channel, isPod, coupling, range, offset, prescale) ) Py_RETURN_TRUE; else Py_RETURN_FALSE;
}

#define BL_TRIG_RISE 1
#define BL_TRIG_FALL 0
#define BL_TRIG_EXT  64

static PyObject * bitlib_BL_SetupTrigger ( PyObject * self, PyObject * args ) {
	int channel; double level; int edge;
	if ( !PyArg_ParseTuple(args, "idi:BL_SetupTrigger", &channel, &level, &edge) ) return NULL;
	if ( BL_SetupTrigger(channel, level, edge)) Py_RETURN_TRUE; else Py_RETURN_FALSE;
}

static PyObject * bitlib_BL_SetTriggerDelay( PyObject * self, PyObject * args ) {
	int delay = 0;
	if ( !PyArg_ParseTuple(args, "i:BL_SetTriggerDelay", &delay) ) return NULL;
	return PyInt_FromLong( (long) BL_SetTriggerDelay(delay) );
}

static PyObject * bitlib_BL_SetAddress( PyObject * self, PyObject * args ) {
	int addr = 0;
	if ( !PyArg_ParseTuple(args, "i:BL_SetAddress", &addr) ) return NULL;
	if ( (long) BL_SetAddress(addr)) Py_RETURN_TRUE; else Py_RETURN_FALSE;
}

#define BL_MAX_RATE -1

static PyObject * bitlib_BL_SampleRate( PyObject * self, PyObject * args ) {
	double rate = 0;
	if ( !PyArg_ParseTuple(args, "|d:BL_SampleRate", &rate) ) return NULL;
	return PyFloat_FromDouble( BL_SampleRate(rate) );
}

#define BL_MAX_TIME -1

static PyObject * bitlib_BL_CaptureTime( PyObject * self, PyObject * args ) {
	int time = 0;
	if ( !PyArg_ParseTuple(args, "|i:BL_CaptureTime", &time) ) return NULL;
	return PyInt_FromLong( (long) BL_CaptureTime(time) );
}

static PyObject * bitlib_BL_PreTrigger( PyObject * self, PyObject * args ) {
	int time;
	if ( !PyArg_ParseTuple(args, "i:BL_PreTrigger", &time) ) return NULL;
	return PyInt_FromLong( (long) BL_PreTrigger(time) );
}

static PyObject * bitlib_BL_BufferSize ( PyObject * self, PyObject * args ) {
	return PyInt_FromLong( (long) BL_BufferSize() );
}

#define BL_ASYNCHRONOUS 1
#define BL_SYNCHRONOUS 0
#define BL_TRACE_FORCED 0
#define BL_TRACE_FOREVER (-1)
#define BL_TRACE_NEVER (-2)

static PyObject * bitlib_BL_Trace( PyObject * self, PyObject * args ) {
	int timeout = 0; bool async = 0;
	if ( !PyArg_ParseTuple(args, "i|b:BL_Trace", &timeout, &async) ) return NULL;
	if( BL_Trace(timeout, async) ) Py_RETURN_TRUE; else Py_RETURN_FALSE;
}

#define BL_STATE_CAPTURED 2
#define BL_STATE_ARMED 1
#define BL_STATE_TIMEDOUT 3
#define BL_STATE_IDLE 0

static PyObject * bitlib_BL_GetTraceState( PyObject * self, PyObject * args ) {
	return PyInt_FromLong( (int) BL_GetTraceState() );
}

static PyObject * bitlib_BL_Halt ( PyObject * self, PyObject * args ) {
	int handle = 0;
	if ( !PyArg_ParseTuple(args, "i:BL_Halt", &handle) ) return NULL;	
	if ( BL_Halt(handle) ) Py_RETURN_TRUE;  else Py_RETURN_FALSE;
}

static PyObject * bitlib_BL_Acquire ( PyObject * self, PyObject * args ) {
	PyArrayObject * array = NULL; int numRequested = 0; int num; bool returnArray = 0;
	if ( ! PyArg_ParseTuple(args, "|iO!", &numRequested, &PyArray_Type, &array) )
		return NULL;
	
	if ( array ) {
		if (array->descr->type_num != NPY_DOUBLE || array->nd !=1) {
			PyErr_SetString(PyExc_ValueError, "Second argument should be a vector of type Float."); 
			return NULL;
		}
		if (array->dimensions[0] < numRequested) {
			PyErr_SetString(PyExc_ValueError, "Second argument is too small to contain requested data."); 
			return NULL;		
		}
		if ( numRequested > 0 ) numRequested = min(numRequested, array->dimensions[0]);
		else numRequested = array->dimensions[0];
	}

	if ( numRequested > 0 ) num = numRequested;
	else num = BL_BufferSize();
	if ( num <= 0 ) {
		PyErr_SetString(PyExc_TypeError, "Acquisition size is unknown."); 
		return NULL;
	}
	
	if ( !array ) {
		int dims[2];
		dims[0]=num;
		array = (PyArrayObject *) PyArray_FromDims(1, dims, NPY_DOUBLE);
		returnArray = 1;
	}

	if ( BL_Acquire( num, (double*) array->data ) ) {
		if ( returnArray ) return PyArray_Return(array);
		else return PyInt_FromLong( (long) num );
	} else {
		PyErr_SetString(PyExc_RuntimeError, "Acquisition failed."); 
		return NULL;
	}
}

static PyObject * bitlib_BL_Close ( PyObject * self, PyObject * args ) {
	BL_Close();
	Py_RETURN_TRUE;
}

#define BL_SOURCE_POD 0
#define BL_SOURCE_BNC 1
#define BL_SOURCE_PRESCALE1 2
#define BL_SOURCE_PRESCAEL2 3
#define BL_SOURCE_GND 4

static PyObject * bitlib_BL_SourcesCount ( PyObject * self, PyObject * args ) {
	int handle = 0; int ad;
	if ( !PyArg_ParseTuple(args, "ii:BL_SourcesCount", &handle, &ad) ) return NULL;	
	return PyInt_FromLong( (long) BL_SourcesCount(handle, ad) );
}

static PyObject * bitlib_BL_ChannelCount ( PyObject * self, PyObject * args ) {
	int handle; int source; int ad;
	if ( !PyArg_ParseTuple(args, "iii:BL_ChannelCount",&handle, &source, &ad) ) return NULL;	
	return PyInt_FromLong( (long) BL_ChannelCount(handle, source, ad) );
}

static PyObject * bitlib_BL_RangesCount ( PyObject * self, PyObject * args ) {
	int handle; int channel; int source; int ad;
	if ( !PyArg_ParseTuple(args, "iiii:BL_RangesCount",&handle, &channel, &source, &ad) ) return NULL;	
	return PyInt_FromLong( (long) BL_RangesCount(handle, channel, source, ad) );
}

static PyObject * bitlib_BL_RangeValue ( PyObject * self, PyObject * args ) {
	int handle; int channel; int source; int range; int ad;
	if ( !PyArg_ParseTuple(args, "iiiii:BL_RangeValue",&handle, &channel, &source, &range, &ad) ) return NULL;	
	return PyFloat_FromDouble( BL_RangeValue(handle, channel, source, range, ad) );
}

static PyObject * bitlib_BL_RangeReference ( PyObject * self, PyObject * args ) {
	int handle; int channel; int source; int range; int ad;
	if ( !PyArg_ParseTuple(args, "iiiii:BL_RangeReference",&handle, &channel, &source, &range, &ad) ) return NULL;	
	return PyFloat_FromDouble( BL_RangeReference(handle, channel, source, range, ad) );
}


static PyMethodDef bitlib_methods[] = { /* method definition table */
	{"BL_Initialize", bitlib_BL_Initialize, METH_VARARGS, ""},
	{"BL_Open", bitlib_BL_Open, METH_VARARGS, ""},
	{"BL_OpenLink", bitlib_BL_OpenLink, METH_VARARGS, ""},
	{"BL_Log", bitlib_BL_Log, METH_VARARGS, ""},
	{"BL_Count", bitlib_BL_Count, METH_VARARGS, ""},
	{"BL_Version", bitlib_BL_Version, METH_VARARGS, ""},
	{"BL_Name", bitlib_BL_Name, METH_VARARGS, ""},
	{"BL_SelectBitScope", bitlib_BL_SelectBitScope, METH_VARARGS, ""},
	{"BL_Mode", bitlib_BL_Mode, METH_VARARGS, ""},
	{"BL_ChannelSelect", bitlib_BL_ChannelSelect, METH_VARARGS, ""},
	{"BL_ChannelEnable", bitlib_BL_ChannelEnable, METH_VARARGS, ""},
	{"BL_SetupChannel", bitlib_BL_SetupChannel, METH_VARARGS, ""},
	{"BL_SetupTrigger", bitlib_BL_SetupTrigger, METH_VARARGS, ""},
	{"BL_SetTriggerDelay", bitlib_BL_SetTriggerDelay, METH_VARARGS, ""},
	{"BL_SetAddress", bitlib_BL_SetAddress, METH_VARARGS, ""},
	{"BL_SampleRate", bitlib_BL_SampleRate, METH_VARARGS, ""},
	{"BL_CaptureTime", bitlib_BL_CaptureTime, METH_VARARGS, ""},
	{"BL_PreTrigger", bitlib_BL_PreTrigger, METH_VARARGS, ""},
	{"BL_BufferSize",bitlib_BL_BufferSize, METH_VARARGS, ""},
	{"BL_Trace",bitlib_BL_Trace, METH_VARARGS, ""},
	{"BL_GetTraceState",bitlib_BL_GetTraceState, METH_VARARGS, ""},
	{"BL_Halt", bitlib_BL_Halt, METH_VARARGS, ""},
	{"BL_Acquire", bitlib_BL_Acquire, METH_VARARGS, ""},
	{"BL_Close", bitlib_BL_Close, METH_VARARGS, ""},
	{"BL_SourcesCount", bitlib_BL_SourcesCount, METH_VARARGS, ""},
	{"BL_ChannelCount", bitlib_BL_ChannelCount, METH_VARARGS, ""},
	{"BL_RangesCount", bitlib_BL_RangesCount, METH_VARARGS, ""},
	{"BL_RangeValue", bitlib_BL_RangeValue, METH_VARARGS, ""},
	{"BL_RangeReference", bitlib_BL_RangeReference, METH_VARARGS, ""},
	{NULL, NULL}
};

PyMODINIT_FUNC initbitlib ( void ) { /* initialise module */
	PyObject * module; int i, n;
	struct { char * N; int V; } T[] = {
		{"BL_MAX_RATE", BL_MAX_RATE},
		{"BL_MAX_TIME", BL_MAX_TIME},
		{"BL_MODE_SINGLE", BL_MODE_SINGLE},
		{"BL_MODE_CHOP", BL_MODE_CHOP},
		{"BL_MODE_MIXED", BL_MODE_MIXED},
		{"BL_MODE_LOGIC", BL_MODE_LOGIC},
		{"BL_MODE_MULTI", BL_MODE_MULTI},
		{"BL_MODE_STREAM", BL_MODE_STREAM},
		{"BL_COUPLING_DC", BL_COUPLING_DC},
		{"BL_COUPLING_AC", BL_COUPLING_AC},
		{"BL_TRIG_FALL", BL_TRIG_FALL},
		{"BL_TRIG_RISE", BL_TRIG_RISE},
		{"BL_TRIG_EXT", BL_TRIG_EXT},
		{"BL_CHANNEL_BNC", BL_CHANNEL_BNC},
		{"BL_CHANNEL_POD", BL_CHANNEL_POD},
		{"BL_PRESCALE_OFF", BL_PRESCALE_OFF},
		{"BL_PRESCALE_ON", BL_PRESCALE_ON},
		{"BL_PRESCALE_GND", BL_PRESCALE_GND},
		{"BL_ASYNCHRONOUS", BL_ASYNCHRONOUS},
		{"BL_SYNCHRONOUS", BL_SYNCHRONOUS},
		{"BL_TRACE_FORCED", BL_TRACE_FORCED},
		{"BL_TRACE_FOREVER", BL_TRACE_FOREVER},
		{"BL_TRACE_NEVER", BL_TRACE_NEVER},
		{"BL_STATE_ARMED", BL_STATE_ARMED},
		{"BL_STATE_CAPTURED", BL_STATE_CAPTURED},
		{"BL_STATE_TIMEDOUT", BL_STATE_TIMEDOUT},
		{"BL_STATE_IDLE", BL_STATE_IDLE},
		{"BL_VERSION_BINDING", BL_VERSION_BINDING},
		{"BL_VERSION_DEVICE", BL_VERSION_DEVICE},
		{"BL_VERSION_LIBRARY", BL_VERSION_LIBRARY},
		{"BL_SOURCE_POD", BL_SOURCE_POD},
		{"BL_SOURCE_BNC", BL_SOURCE_BNC},
		{"BL_SOURCE_PRESCALE1", BL_SOURCE_PRESCALE1},
		{"BL_SOURCE_PRESCAEL2", BL_SOURCE_PRESCAEL2},
		{"BL_SOURCE_GND", BL_SOURCE_GND},
	};
	module = Py_InitModule3("bitlib", bitlib_methods, bitlib_doc);
	n = sizeof(T) / sizeof(T[0]);
	for ( i = 0; i < n; i++ )
		PyModule_AddIntConstant(module, T[i].N, T[i].V);
	import_array();
}
